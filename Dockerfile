FROM alpine:3.1

MAINTAINER  Giménez Silva Germán Alberto

RUN mkdir apt1
VOLUME /apt1
WORKDIR /apt1

ADD . /apt1

ENTRYPOINT ["sh"]
